package com.wings;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HelloWorld {
    
    @Value("${app.username}")
    protected String username;
    
    @GetMapping()
    public String sayHello(){
        return "Hello, welcome to SB and Docker! User = " + username;
    }
}
